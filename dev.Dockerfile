FROM python:3.8

ENV APP_PATH /usr/src/app
ENV API_DIR /usr/src/app/api

RUN mkdir -p $API_DIR

COPY . $APP_PATH

RUN pip install -r $APP_PATH/requirements.txt

WORKDIR $API_DIR

EXPOSE 8080

CMD ["python3", "manage.py", "runserver", "0:8080"]
# uvicorn api.asgi:application