from rest_framework import serializers

from .models import PricingRule


class PricingRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = PricingRule
        fields = [
            'name', 'start_time', 'end_time', 'standing_charge',
            'minute_call_charge']
