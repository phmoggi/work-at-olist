from rest_framework import viewsets

from .models import PricingRule
from .serializers import PricingRuleSerializer


class PricingRuleViewSet(viewsets.ModelViewSet):
    queryset = PricingRule.objects.all()
    serializer_class = PricingRuleSerializer
