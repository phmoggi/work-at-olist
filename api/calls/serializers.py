from rest_framework import serializers

from .models import Call, CallDetail


class CallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Call
        fields = ['detail_start', 'detail_end', 'price']


class CallDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallDetail
        fields = ['type', 'timestamp', 'call_id', 'source', 'destination']
