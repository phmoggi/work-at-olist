from django.contrib import admin

from .models import Call, CallDetail

admin.site.register(Call)
admin.site.register(CallDetail)
