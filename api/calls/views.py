from rest_framework import viewsets

from .models import Call, CallDetail
from .serializers import CallSerializer, CallDetailSerializer


class CallViewSet(viewsets.ModelViewSet):
    queryset = Call.objects.all()
    serializer_class = CallSerializer


class CallDetailViewSet(viewsets.ModelViewSet):
    queryset = CallDetail.objects.all()
    serializer_class = CallDetailSerializer
