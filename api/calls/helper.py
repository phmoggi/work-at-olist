
import re

from datetime import timedelta, datetime


def time_between(timestamp_start: datetime, timestamp_end: datetime) -> str:
    """
    Return delta time between two timestamps as a `{00}h{00}m{00}s`
    """
    delta = timestamp_end - timestamp_start
    hours, remainder = divmod(delta.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    hours += delta.days * 24
    return f'{hours}h{minutes}m{seconds}s'


def to_timedelta(date: datetime) -> timedelta:
    """
    Convert datetime to allow subtraction operations.
    """
    time = {'hours': date.hour,
            'minutes': date.minute,
            'seconds': date.second}

    return timedelta(**time)


def current_month_year() -> datetime:
    """
    Returns a datetime of the first day of the current month and year.
    """
    today = datetime.now()
    return datetime(year=today.year, month=today.month, day=1)


def last_month_year() -> datetime:
    """
    Returns a datetime of to the first day of the last month and year.
    """
    today = datetime.now()
    first = today.replace(day=1)
    last_month = first - timedelta(days=1)

    return datetime(year=last_month.year, month=last_month.month, day=1)


def valid_phone_number(phone_number: str) -> bool:
    """
    Return True if telephone has a valid format.
    Only digits with length between 10 and 11.
    """
    pattern = re.compile(r'^\d{10,11}$')

    return pattern.match(phone_number) is not None
