
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers
from pricing.views import PricingRuleViewSet
from calls.views import CallViewSet, CallDetailViewSet

router = routers.DefaultRouter()
router.register('call', CallViewSet)
router.register('call_detail', CallDetailViewSet)
router.register('pricing_rule', PricingRuleViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
]
